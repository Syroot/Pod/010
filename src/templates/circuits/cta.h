#ifndef CTA_H
#define CTA_H

#include "../math.h"

struct TexAniFile;
struct TexAniKey;
struct TexAniStep;

typedef struct // TexAniFile (.cta)
{
	int keyCount; // "Nombres de clefs"
	TexAniKey keys[keyCount]; // "Clefs"

	fix duration; // "Duree"

	int stepCount; // "Nombres d'etapes"
	TexAniStep syncCurve[stepCount]; // "Courbe de synchro"
} TexAniFile;

typedef struct // TexAniKey
{
	int textureIndex; // ushort
	v2u uv[4]; // ubytes
} TexAniKey;

typedef struct // TexAniStep
{
	fix time;
	int keyIndex;
} TexAniStep;

#endif
