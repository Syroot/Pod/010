#ifndef RSO_H
#define RSO_H

#include "../math.h"
#include "../pbdf.h"

struct MapConstraint;
struct MapFile;
struct MapSection;
struct MapTrail;

typedef struct // MapFile (.rso)
{
	if (OEM)
	{
		int unknownCount;
		int unknowns[unknownCount];
	}

	int positionCount; // "Nombre d'enregistrements"
	v3fix positions[positionCount]; // "Table des trajectoires des competitors"

	int sectionCount; // "Nb de troncons"
	MapSection sections[sectionCount] <optimize=false>;

	pstring constraintName;
	if (PStr(constraintName) == "PLANS CONTRAINTES")
	{
		int constraintCount; // "Nb plans contraintes"
		MapConstraint constraints[constraintCount];
	}
} MapFile;

typedef struct // MapSection
{
	int trailCount; // "Nb de pistes"
	MapTrail trails[trailCount];
} MapSection;

typedef struct // MapTrail
{
	int level; // "Niveau", ushort
	int positionCount; // "Nb_Points", ushort
	int positionStart; // "Index_Debut"
	enum
	{
		CONSTRAINT_NONE,
		CONSTRAINT_MINSPEED,
		CONSTRAINT_MAXSPEED,
		CONSTRAINT_STOPATTACK,
		CONSTRAINT_DISENGAGE,
		CONSTRAINT_PIT,
		CONSTRAINT_START,
	} constraintType : 4; // "Contrainte"
	enum
	{
		GRAPH_NONE,
		GRAPH_PREFER,
		GRAPH_IGNORE,
		GRAPH_ADDITIONAL,
	} constraintGraph : 2;
	int hidden : 1;
	int disabled : 1;
	int constraintNumber : 8;
	int constraintParam : 16;
} MapTrail <read=MapTrailRead>;
string MapTrailRead(MapTrail& value)
{
	return Str("Level %d, Positions[%d-%d]%s",
		value.level, value.positionStart, value.positionStart + value.positionCount,
		value.disabled ? ", disabled" : "");
}

typedef struct // MapConstraint
{
	int beaconIndex; // "No_Balise"
	int guideIndex; // "No_Guide"
	int constraint; // "Contrainte"
} MapConstraint;

#endif
