#ifndef PHA_H
#define PHA_H

#include "../pbdf.h"
#include "common.h"

struct Phase;
struct PhaseFile;

typedef struct // PhaseFile (.pha)
{
	EventList eventList;

	int phaseCount;
	Phase phases[phaseCount] <optimize=false>;
} PhaseFile;

typedef struct // Phase
{
	pstring name; // "Nombre de phases de jeu", "Arrivee", "Charge", "Depart", "FinCourse"
	int eventIndex;
} Phase <read=PhaseRead>;
string PhaseRead(Phase& value)
{
	return PStr(value.name);
}

#endif
