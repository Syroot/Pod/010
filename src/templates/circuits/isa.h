#ifndef ISA_H
#define ISA_H

#include "../math.h"
#include "csa.h"

struct SectorAniInfo;
struct SectorAniInfoFile;

typedef struct // SectorAniInfoFile (.isa)
{
    int infoCount;
    SectorAniInfo infos[infoCount] <optimize=false>;
} SectorAniInfoFile;

typedef struct // SectorAniInfo
{
    int sectorIndex;
    SectorAniFile file;
    int subCount;
    int unknown10;
    v2u subs[subCount];
} SectorAniInfo;

#endif
