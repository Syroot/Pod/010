#ifndef ZRP_H
#define ZRP_H

#include "../math.h"

struct RepairZone;
struct RepairZoneFile;

typedef struct // RepairZoneFile (.zrp)
{
	int count; // "Nb zones de reparations"
	RepairZone repairZones[count];
	fix repairSeconds;
} RepairZoneFile;

typedef struct // RepairZone
{
	v3fix positions[4];
	v3fix center;
	fix height;
	fix size;
} RepairZone;

#endif
