#ifndef ANC_H
#define ANC_H

#include "../math.h"
#include "../object.h"
#include "../pbdf.h"
#include "../texture.h"
#include "ani.h"
#include "common.h"

struct AnimFile;
struct AnimInstance;
struct AnimInstanceEvent;
struct AnimInstanceList;

typedef struct // AnimFile (.anc)
{
	EventList eventList;

	int animCount; // "Nombre+Noms des anims pour l'environnement decor"
	struct AnimInfo
	{
		pstring filename;
		if (PStr(filename) == "wrongway.ani")
		{
			short wrongway0;
			short gap <hidden=true>;
			short wrongway1;
			short gap <hidden=true>;
		}
		Anim anim <open=true>;
	} anims[animCount] <optimize=false, read=AnimInfoRead>;

	int instanceCountTotal;
	AnimInstanceList globalInstances; // "GLOBALES"
	AnimInstanceList sectorInstances[circuit.routeFile.sectorCount] <optimize=false>;
} AnimFile;
string AnimInfoRead(AnimInfo& value)
{
	return PStr(value.filename);
}

typedef struct // AnimInstanceList
{
	int instanceCount; // "nombre d'anims"
	int eventCount; // "nb d'evenements"
	AnimInstance instances[instanceCount] <optimize=false>;
} AnimInstanceList;

typedef struct // AnimInstance
{
	int descriptor; // "Descripteur", ani index
	int triggerMode; // "mode de declenchement", ushort
	int progress; // "deroulement", ushort
	int display; // "affichage", ushort
	int speed; // "vitesse", ushort
	int eventCount; // "nombre d'evenements associes a l'animation"
	AnimInstanceEvent events[eventCount];
	v3fix position; // "vecteur translation"
	m33fix rotation; // "matrice rotation"
} AnimInstance;

typedef struct // AnimInstanceEvent
{
	int eventIndex; // "numero du descripteur"
	int frameCount; // "numero de frame"
} AnimInstanceEvent;

#endif
