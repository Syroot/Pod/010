#ifndef CSA_H
#define CSA_H

#include "../object.h"

struct SectorAniFile;

typedef struct // SectorAniFile (.csa?)
{
    int hasFaceMaterial; // bool
    int objectCount;
    Object objects(hasFaceMaterial, false, true)[objectCount] <optimize=false>;
} SectorAniFile;

#endif
