#ifndef RTE_H
#define RTE_H

#include "../math.h"
#include "../object.h"
#include "../texture.h"

struct Bbox;
struct RouteFile;
struct Sector;

typedef struct // RouteFile (.rte)
{
	pstring textureProjectName;
	TextureProject textureProject(256);

	int hasSectorFaceMaterial; // bool
	int sectorCount;
	Sector sectors(hasSectorFaceMaterial)[sectorCount] <optimize=false>;
} RouteFile;

typedef struct(uint hasFaceMaterial) // Sector
{
	Object object(hasFaceMaterial, VOODOO && BL != 9, BL != 7);
	ubyte vertexBrightness[object.vertexCount]; // clamps to 0 - 127, 0=black (-1), 63=normal (0), 127=double (1)
	Bbox bbox;
} Sector;

typedef struct // Bbox
{
	v3fix min; // z -= 2
	v3fix max; // z += 10
} Bbox;

#endif
