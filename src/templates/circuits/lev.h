#ifndef LEV_H
#define LEV_H

#include "../pbdf.h"
#include "anc.h"
#include "bal.h"
#include "common.h"
#include "con_.h"
#include "env.h"
#include "gri.h"
#include "hp.h"
#include "isa.h"
#include "ita.h"
#include "lum.h"
#include "mev.h"
#include "pha.h"
#include "pit.h"
#include "pj.h"
#include "rso.h"
#include "rte.h"
#include "tev.h"
#include "tim.h"
#include "voo.h"
#include "zon.h"
#include "zrp.h"

struct conc;
struct diff;
struct dir;
struct Circuit;

typedef struct // Circuit (.lev)
{
	if (!OEM)
	{
		int check; // 3
		int unused;
	}
	EventTypeFile eventTypeFile <bgcolor=0xCDE6FF>;
	EventMacroFile eventMacroFile <bgcolor=0xCDE6FF>;

	if (OEM)
		TimeFile timeFile;
	if (BL < 8)
		pstring routeFilename <bgcolor=0xCDCDFF>; // "Nom du fichier RTE"
	if (VOODOO)
        VoodooFile voodooFile <bgcolor=0xCDCDFF>;
	RouteFile routeFile <bgcolor=0xCDCDFF>;
	ZoneFile zoneFile <bgcolor=0xCDCDFF>;

	pstring environmentFilename <bgcolor=0xEFCDFF>; // "Nom du fichier ENV"
	if (HasFile(environmentFilename))
		EnvironmentFile environmentFile <bgcolor=0xEFCDFF>;

	if (BL < 8)
	{
		pstring lightFilename <bgcolor=0xFFCDEF>; // "Nom du fichier LUM"
		if (HasFile(lightFilename))
			LightFile lightFile <bgcolor=0xFFCDEF>;

		pstring animFilename <bgcolor=0xFFCDCD>; // "Nom du fichier ANC"
		if (HasFile(animFilename))
			AnimFile animFile <bgcolor=0xFFCDCD>;
	}
	else
	{
		AnimFile animFile <bgcolor=0xFFCDCD>;
		LightFile lightFile <bgcolor=0xFFCDEF>;
	}

	if (OEM)
	{
		pstring beaconFilename <bgcolor=0xBDEBDC>; // "Nom du fichier BAL"
		if (HasFile(beaconFilename))
		{
			BeaconFile beaconFile <bgcolor=0xBDEBDC>;
			PhaseFile phaseFile <bgcolor=0xBDEBDC>;
		}
	}

	if (BL < 6)
	{
		pstring speakerFilename <bgcolor=0xFFD6CD>; // "Nom du fichier HP"
		if (HasFile(speakerFilename))
			SpeakerFile speakerFile <bgcolor=0xFFD6CD>;

		World world <bgcolor=0xFFEFCD>;
	}
	else
	{
		World world <bgcolor=0xFFEFCD>;

		pstring speakerFilename <bgcolor=0xFFD6CD>; // "Nom du fichier HP"
		if (HasFile(speakerFilename))
			SpeakerFile speakerFilename <bgcolor=0xFFD6CD>;
	}
	World2 world2 <bgcolor=0xFFEFCD>;

	int aniFileCount <bgcolor=0xEFFFCD>;
	local int i;
	for (i = 0; i < aniFileCount; i += 2)
	{
		pstring aniInfoFilename <bgcolor=0xEFFFCD>;
		if (PStr(aniInfoFilename) == "ANIME SECTEUR")
			SectorAniInfoFile aniInfoFile <bgcolor=0xEFFFCD>;
		else
			TexAniInfoFile aniInfoFile <bgcolor=0xEFFFCD>;
	};

	if (OEM)
	{
		pstring gateFilename <bgcolor=0xBDEBDC>;
		if (HasFile(gateFilename))
			GateFile gateFile <bgcolor=0xBDEBDC>;

		pstring pitFilename <bgcolor=0xCDFFCD>;
		if (HasFile(pitFilename))
			PitFile pitFile <bgcolor=0xCDFFCD>;

		diff diff_EASY(1);
		diff diff_MEDIUM(2);
		diff diff_HARD(3);
	}
	else
	{
		pstring repairZoneFilename <bgcolor=0xCDFFCD>;
		if (HasFile(repairZoneFilename))
			RepairZoneFile repairZoneFile <bgcolor=0xCDFFCD>;

		dir dir_FORWARD(0) <bgcolor=0xCDFFEF>;
		dir dir_REVERSE(3) <bgcolor=0xCDFFEF>;

		conc conc_EASY(6) <bgcolor=0xE9E9E9>;
		conc conc_MEDIUM(7) <bgcolor=0xE9E9E9>;
		conc conc_HARD(8) <bgcolor=0xE9E9E9>;
	}
} Circuit <bgcolor=0xCDFFFF>;

typedef struct(int offset) // dir
{
	if (offset != 0 && !ARCADE)
		PbdfSeekOffset(offset);

	TimeFile timeFile;

	pstring beaconFilename <bgcolor=0xBDEBDC>; // "Nom du fichier BAL"
	if (HasFile(beaconFilename))
	{
		BeaconFile beaconFile <bgcolor=0xBDEBDC>;
		PhaseFile phaseFile <bgcolor=0xBDEBDC>;
	}

	GateFile gateFile <bgcolor=0xBDEBDC>;

	diff diff_EASY(0);
	diff diff_MEDIUM(offset + 1);
	diff diff_HARD(offset + 2);
} dir;

typedef struct(int offset) // diff
{
	if (offset != 0 && !ARCADE)
		PbdfSeekOffset(offset);

	pstring difficultyName;

	pstring mapFilename <bgcolor=0xAED9CB>;
	if (HasFile(mapFilename))
		MapFile mapFile <bgcolor=0xAED9CB>;

	pstring pathFilename <bgcolor=0xA1C8BB>;
	if (HasFile(pathFilename))
		PathFile pathFile <bgcolor=0xA1C8BB>;

	if (OEM)
	{
		pstring competitorFilename;
		if (HasFile(competitorFilename))
			CompetitorFile competitorFile;
	}
} diff;

typedef struct(int offset) // conc
{
	if (!ARCADE)
		PbdfSeekOffset(offset);

	pstring difficultyName;

	pstring competitorFilename;
	if (HasFile(competitorFilename))
		CompetitorFile competitorFile;
} conc;

#endif
