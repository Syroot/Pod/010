#ifndef LUM_H
#define LUM_H

#include "../light.h"
#include "../math.h"

struct Light;
struct LightFile;
struct LightList;

typedef struct // LightFile (.lum)
{
	int sectorCount; // "nombre de secteur"
	int objectAmbient; // "Paramètres influants sur la voiture et les objets > Ambiante"
	v3fix direction; // "Vecteur lumière"
	int circuitLight; // "Paramètres influants sur le circuit > Light"
	int circuitDark; // "Paramètres influants sur le circuit > Dark"
	int objectTunnel; // "Paramètres influants sur la voiture et les objets > Tunnel"
	int objectShadow; // "Paramètres influants sur la voiture et les objets > Ombre"
	if (!OEM)
	{
		ubyte limitR[4]; // "Couleur des palettes" 0="Borne Inf" lower, 1="Borne Sup" upper
		ubyte limitG[4]; // "Couleur des palettes" 0="Borne Inf" lower, 1="Borne Sup" upper
		ubyte limitB[4]; // "Couleur des palettes" 0="Borne Inf" lower, 1="Borne Sup" upper
	}
	LightList globalLights; // "Lumieres Globales"
	LightList sectorLights[sectorCount] <optimize=false>; // "Lumieres du secteur"
} LightFile;

typedef struct // LightList
{
	int lightCount;
	Light lights[lightCount];
} LightList;

#endif
