#ifndef PJ_H
#define PJ_H

#include "../math.h"
#include "../utils.h"

struct PathArc;
struct PathFile;
struct PathNeighbor;
struct PathNode;

typedef struct // PathFile (.pj)
{
	fix circuitLength; // "Longueur du circuit"
	int nodeCount; // "Nombre de noeuds", = MapFile.sectionCount
	int arcCount; // "Nombre d arcs"
	if (!OEM)
	{
		int neighborCount; // "Nombre de voisinages"
		int allocSize; // "Nb <long> Alloc", node.outArcCount + node.inArcCount + arc.trailCount + arc.pointCount
	}
	PathNode nodes[nodeCount] <optimize=false>;
	PathArc arcs[arcCount] <optimize = false>;
	if (!OEM)
		PathNeighbor neighbors[neighborCount];
} PathFile;

typedef struct // PathNode
{
	v3fix position; // "Position"
	fix distance; // "Distance"
	if (!OEM)
		int neighbor; // "Voisinage", 0-based, optional

	int outArcCount; // "Nb Arcs Sortants"
	int outArcs[outArcCount]; // 1-based

	int inArcCount; // "Nb Arcs Rentrants"
	int inArcs[inArcCount]; // 1-based
} PathNode <read=PathNodeRead>;
string PathNodeRead(PathNode& value)
{
	int i;
	int inArcs[value.inArcCount];
	for (i = 0; i < value.inArcCount; ++i)
		inArcs[i] = value.inArcs[i] - 1;
	int outArcs[value.outArcCount];
	for (i = 0; i < value.outArcCount; ++i)
		outArcs[i] = value.outArcs[i] - 1;

	string s;
	s += Str("Distance=%.02f", fix2float(value.distance));
	s += Str(" InArcs=%s", value.inArcCount ? SPrintfIntArray(inArcs, value.inArcCount) : "[]");
	s += Str(" OutArcs=%s", value.outArcCount ? SPrintfIntArray(outArcs, value.outArcCount) : "[]");
	if (exists(value.neighbor) && value.neighbor != -1)
		s += Str(" Neighbor=%d", value.neighbor);
	return s;
}

typedef struct // PathArc
{
	int parentNode; // "Noeud Pere", 1-based, required
	int childNode; // "Noeud Fils", 1-based, required

	int trailCount; // "Nb Pistes couvertes"
	int trails[trailCount]; // 0-based, see MapFile.sections[parentNode - 1].trails[]

	int distanceTrail; // "Piste de reference", 0-based index in trails[], required
	int distanceCount; // "Nb points sur arc", = MapFile.sections[parentNode - 1].trails[distanceTrail].positionCount
	fix distances[distanceCount]; // "Distance point"
} PathArc <read=PathArcRead>;
string PathArcRead(PathArc& value)
{
	string s;
	s += Str("ParentNode=%d", value.parentNode - 1);
	s += Str(" ChildNode=%d", value.childNode - 1);
	s += Str(" DistanceTrail=%d", value.trails[value.distanceTrail]);
	s += Str(" Trails=%s", SPrintfIntArray(value.trails, value.trailCount));
	return s;
}

typedef struct // PathNeighbor
{
	int initialNode; // "Noeud Initial", 1-based, required
	int finalNode; // "Noeud Final", 1-based, optional
	int parentNeighbor; // "Voisinage Pere", 0-based, optional
} PathNeighbor <read=PathNeighborRead>;
string PathNeighborRead(PathNeighbor& value)
{
	string s;
	s += Str("InitialNode=%d", value.initialNode - 1);
	s += Str(" FinalNode=%d", value.finalNode - 1);
	if (value.parentNeighbor != -1)
		s += Str(" ParentNeighbor=%d", value.parentNeighbor);
	return s;
}

#endif
