#ifndef CON_H
#define CON_H

#include "../pbdf.h"

struct Competitor;
struct CompetitorAttack;
struct CompetitorFile;

typedef struct // CompetitorFile (.con)
{
	int competitorCount; // "Nombre de competitors"
	Competitor competitors[competitorCount] <optimize=false>;
} CompetitorFile;

typedef struct // Competitor
{
	pstring name; // "Nom du Competitor"
	int level; // "Niveau Experience"
	enum
	{
		BEHAVIOR_PASSIVE = 1,
		BEHAVIOR_DEFENSIVE,
		BEHAVIOR_AGGRESSIVE,
	} behavior; // "Type de Comportement"
	pstring carIndex; // "Nom de la voiture", textual integer

	int attackCount; // "Nombre d'attaques disponibles"
	CompetitorAttack attacks[attackCount];
} Competitor <read=CompetitorRead>;
string CompetitorRead(Competitor& value)
{
	return PStr(value.name);
}

typedef struct // CompetitorAttack
{
	enum
	{
		ATTACK_DETACH = 1,
		ATTACK_PASSRIGHT = 2,
		ATTACK_PASSLEFT = 3,
		ATTACK_TAILRIGHT = 5,
		ATTACK_TAILLEFT = 6,
		ATTACK_ZIGZAG = 7,
		ATTACK_HITFRONT = 8,
		ATTACK_ACCEL = 10,
		ATTACK_BRAKE = 11,
		ATTACK_HITRIGHT = 12,
		ATTACK_HITLEFT = 13,
		ATTACK_ERROR = 15,
		ATTACK_EVENT = 16,
	} type; // "Attaque", byte
	int contextFront : 1; // "Contexte", byte
	int contextLeft : 1;
	int contextRight : 1;
	int contextRear : 1;
	int percentage; // "Pourcentage", byte
} CompetitorAttack;

#endif
