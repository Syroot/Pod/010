#ifndef A3D_H
#define A3D_H

#include "../pbdf.h"
#include "../object.h"

struct Ani3D;
struct Ani3DFrame;

typedef struct // Ani3D (.a3d)
{
	int startFrame;
	int frameCount; // "Nombre d'images de l'animation"
	int hasFaceMaterial;
	int objectCount; // "Nombre d'objets de l'animation"
	pstring texturePage; // "Page de texture"
	Object objects(hasFaceMaterial, false, true)[objectCount] <optimize=false>;
	struct
	{
		Ani3DFrame frames[frameCount] <optimize=false>;
	} objectFrames[objectCount] <optimize=false>;
} Ani3D;

typedef struct // Ani3DFrame
{
	int hasTransform; // is not "H"
	if (hasTransform)
	{
		v3fix position; // "Vecteur de translation"
		m33fix rotation; // "Matrice de rotation"
	}
} Ani3DFrame;

#endif
