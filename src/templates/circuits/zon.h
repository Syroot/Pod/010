#ifndef ZON_H
#define ZON_H

struct ZoneFile;
struct ZoneSector;

typedef struct // ZoneFile (.zon)
{
	int sectorCount; // "Nombre de secteurs"
	ZoneSector sectors[sectorCount] <optimize=false>;
} ZoneFile;

typedef struct // ZoneSector
{
	int visibleSectorCount; // "Nb_Secteurs_Visibles", -1 = no sectors are visible
	if (visibleSectorCount > 0)
		int visibleSectors[visibleSectorCount];
} ZoneSector;

#endif
