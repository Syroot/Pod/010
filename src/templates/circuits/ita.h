#ifndef ITA_H
#define ITA_H

#include "../math.h"
#include "cta.h"

struct TexAni;
struct TexAniInfoFile;
struct TexAniPoly;
struct TexAniSector;

typedef struct // TexAniInfoFile (.ita)
{
	int aniFileCount; // "Nb animations de reference"
	local int i;
	for (i = 0; i < aniFileCount; ++i)
	{
		pstring aniFilename;
		TexAniFile aniFile;
	}

	int polyCountTotal; // "Nombre de polygones concernes"

	int aniCount; // "Nb animations", <= aniFileCount
	TexAni anis[aniCount] <optimize=false>;
} TexAniInfoFile;

typedef struct // TexAni
{
	int aniIndex; // "numero de l'animation"
	int sectorCount; // "nb secteurs concernes"
	TexAniSector sectors[sectorCount] <optimize=false>;
} TexAni;

typedef struct // TexAniSector
{
	int sectorIndex; // "num secteur"
	int polyCount; // "nb polygones"
	TexAniPoly polys[polyCount];
} TexAniSector;

typedef struct // TexAniPoly
{
	int loop; // bool
	int indexCount; // 3 or 4 vertices
	int faceIndex;
} TexAniPoly;

#endif
