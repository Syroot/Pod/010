#ifndef HP_H
#define HP_H

#include "../math.h"
#include "../sound.h"

struct Speaker;
struct SpeakerFile;

typedef struct // SpeakersFile (.hp)
{
	int speakerCount; // "Nombre d emetteurs"
	Speaker speakers[speakerCount];
} SpeakerFile;

typedef struct // Speaker
{
	int exist; // "Existant"
	v3fix position; // "x y z"
	m33fix rotation;
	SoundEvent soundId; // "IdSon"
	ushort gap <hidden=true>;
} Speaker <read=SoundEventRead(this.soundId)>;

#endif
