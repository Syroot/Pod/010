#ifndef TEV_H
#define TEV_H

#include "../pbdf.h"

struct EventType;
struct EventTypeFile;
struct EventTypeParam;

string GetOemEventTypeName(int index)
{
	switch (index)
	{
		case 0: return "...Sinon...";
		case 1: return "Active Macro";
		case 2: return "Aff Env OFF";
		case 3: return "Aff Env ON";
		case 4: return "All Macros OFF";
		case 5: return "All Macros ON";
		case 6: return "Arret Anim";
		case 7: return "Bloque Anim";
		case 8: return "Contrainte";
		case 9: return "Debug balise";
		case 10: return "DEBUT-COND";
		case 11: return "Decision PIT";
		case 12: return "Deplace Anim";
		case 13: return "Desactive Macro";
		case 14: return "Echange Macros";
		case 15: return "Fige Anim";
		case 16: return "Fin Course";
		case 17: return "Fin Dep L";
		case 18: return "FIN-COND";
		case 19: return "Init Macro";
		case 20: return "Lance Anim";
		case 21: return "Macro";
		case 22: return "N-Chrono Inter";
		case 23: return "N-Chrono Tour";
		case 24: return "P-Chrono Inter";
		case 25: return "P-Chrono Tour";
		case 26: return "Pistes cachees";
		case 27: return "Reinit Anim";
		case 28: return "Relance Anim";
		case 29: return "Remplace Macro";
		case 30: return "Reset Reverb Voit";
		case 31: return "Reverb Voiture";
		case 32: return "Son Localisé";
		case 33: return "Son Source";
		case 34: return "Son Statique";
		case 35: return "Stop Anim";
		case 36: return "Troncon";
		case 37: return "Tue Sons Localisés";
		case 38: return "Tue Sons Source";
		case 39: return "Tue Sons Statiques";
		case 40: return "Wrong-Way OFF";
		case 41: return "Wrong-Way ON";
		case 42: return "}}} (Rupture)";
	}
}

int GetOemEventTypeParamSize(int index)
{
	switch (index)
	{
		case 1: return 4;
		case 6: return 44;
		case 12: return 12;
		case 13: return 4;
		case 14: return 8;
		case 15: return 44;
		case 19: return 4;
		case 20: return 44;
		case 21: return 8;
		case 29: return 8;
		case 30: return 4;
		case 31: return 4;
		case 32: return 4;
		case 33: return 4;
		case 34: return 4;
		case 37: return 4;
		default: return 0;
	}
}

typedef struct // EventTypeFile (.tev)
{
	if (OEM)
	{
		local int i;
		for (i = 0; i < 43; ++i)
			EventType types(i);
	}
	else
	{
		int typeCount; // "Nombre de types d'evenements"
		int paramSizeTotal; // total size of all event parameter data (.pev file size)
		EventType types(-1)[typeCount] <optimize=false>;
	}
} EventTypeFile;

typedef struct(int index) // EventType
{
	if (OEM)
	{
		local string name = GetOemEventTypeName(index);
		local int paramSize <format=hex> = GetOemEventTypeParamSize(index);
	}
	else
	{
		pstring typeName; // "Nom du type d evenement"
		local string name = PStr(typeName);
		int paramSize <format=hex>; // "Dimension d un parametrage"
	}
	int paramCount; // "nombre de parametrages"
	EventTypeParam params(paramSize)[paramCount];
} EventType <read=EventTypeRead>;
string EventTypeRead(EventType& value)
{
	if (value.paramSize)
		return Str("%s (%d params)", value.name, value.paramCount);
	return value.name;
}

typedef struct(int size) // EventTypeParam
{
	switch (size)
	{
		case 4:
			int value;
			break;
		case 8:
			int value1;
			int value2;
			break;
		case 12:
			fix x;
			fix y;
			int length;
			break;
		case 44:
			int active; // bool
			int animIndex;
			int sectorIndex;
			char animFileName[32];
			break;
	}
} EventTypeParam;

#endif
