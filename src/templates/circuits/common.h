#ifndef COMMON_H
#define COMMON_H

#include "../pbdf.h"
#include "../texture.h"

struct EventDescriptor;
struct Event;
struct EventList;
struct Sky;
struct World;
struct World2;

bool HasFile(const pstring& name)
{
	return Stricmp(PStr(name), "NEANT");
}

typedef struct // EventList
{
	int eventCount; // "Nombre de descripteurs evenements"
	Event events[eventCount];
} EventList;

typedef struct // Event
{
	int type; // "nom", index of type in event type file
	int param; // "parametrage", index of parameter in event type file
	int link; // "lien (-1 si fin)", index of next event in linked list
} Event <read=EventRead>;
string EventRead(Event& value)
{
	string s = circuit.eventTypeFile.types[value.type].name;
	if (circuit.eventTypeFile.types[value.type].paramCount)
		s += Str(" (p%d)", value.param);
	if (value.link != -1)
		s += Str(", [%d]", value.link);
	return s;
}

typedef struct // World
{
	int fogDistance; // "distance de declenchement du brouillard"
	int fogIntensity; // "intensitee du brouillard du brouillard (negatif pour l'effet de nuit)"
	int fogMistZ; // "Z de declenchement de la brume"
	int fogMistIntensity; // "intensitee de la brume"
	int bgOn; // "FOND_ON = 1 si Fond affiché"
	int bgColor; // "Couleur_fond (0 noir, 1 rouge, 2 vert, 4 bleu, 7 blanc)"
} World;

typedef struct // World2, separated from Background in newer files
{
	pstring bgTextureProjectName;
	TextureProject bgTextureProject(256);
	int bgTextureStart;
	int bgTextureEnd;
	enum
	{
		SKY_NONE,
		SKY_TEXGOU,
		SKY_GOURAUD,
		SKY_TEXGOU_FAST,
		SKY_SHADOWPROJECT,
	} skyType : 4; // "Sky_Switch 0=rien 1=texture_gouraud 2=gouraud 3=texture_gouraud rapide 4 = projection des ombres"
	int skySunOn : 1;
	int skyZ; // "Z du ciel"
	int skyZoom; // "Zoom sur le ciel"
	int skyGouraudIntensity; // "Intensitee du gouraud, -7000 to 7000"
	int skyGouraudStart; // "Debut du gouraud sur le ciel"
	int skySpeed; // "vitesse du vent"
	pstring skyTextureProjectName;
	TextureProject skyTextureProject(VOODOO ? 128 : 256);
	if (VOODOO)
		TexturePage skyLensflareTexture(128);
	int skySunColor; // "Couleur du sol"
	if (ARCADE)
		int skyArcade;
} World2;

#endif
