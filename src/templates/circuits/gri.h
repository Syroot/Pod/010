#ifndef GRI_H
#define GRI_H

#include "../math.h"

struct Gate;
struct GateFile;

typedef struct // GateFile (.gri)
{
	uint gateCount; // "Nombre de position de depart"
	Gate gates[gateCount];
} GateFile;

typedef struct // Gate
{
    v3fix position; // "Position"
    v3fix axisX; // "Axe_X"
    v3fix axisY; // "Axe_Y"
} Gate;

#endif
