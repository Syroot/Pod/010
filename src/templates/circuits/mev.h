#ifndef MEV_H
#define MEV_H

#include "common.h"

struct EventMacroFile;

typedef struct // EventMacroFile (.mev)
{
	EventList eventList;
	int eventCount; // "Nombre de Macro-evenements definis"
	int events[eventCount];
	int initMacroCount; // "Nombre d'ev Initialise-Macro"
	int initMacros[initMacroCount];
	int activeMacroCount; // "Nombre d'ev Active-Macro"
	int activeMacros[activeMacroCount];
	int inactiveMacroCount; // "Nombre d'ev Desactive-Macro"
	int inactiveMacros[inactiveMacroCount];
	int replaceMacroCount; // "Nombre d'ev Remplace-Macro"
	int replaceMacros[replaceMacroCount * 2];
	int exchangeMacroCount; // "Nombre d'ev Echange-Macros"
	int exchangeMacros[exchangeMacroCount * 2];
} EventMacroFile;

#endif
