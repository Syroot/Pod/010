#ifndef ANI_H
#define ANI_H

#include "a2d.h"
#include "a3d.h"

struct Anim;

typedef struct // Anim (.ani)
{
	int ani2DCount; // "NbAni2D", ushort
	int ani3DCount; // "NbAni3D", ushort
	int frameCount; // "NbFrame", ushort
	Ani3D ani3Ds[ani3DCount] <optimize=false>;
	Ani2D ani2Ds[ani2DCount] <optimize=false>;
} Anim;

#endif
