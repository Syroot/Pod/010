#ifndef ENV_H
#define ENV_H

#include "../math.h"
#include "../object.h"
#include "common.h"

struct EnvironmentFile;
struct ObjectInstance;
struct ObjectInstanceEvent;
struct ObjectInstanceList;

typedef struct // EnvironmentFile (.env)
{
	EventList eventList;

	int objectCount; // "Nombre des objets pour l'environnement decor"
	Decor objects(true)[objectCount] <optimize=false>;

	ObjectInstanceList globalInstances;
	ObjectInstanceList sectorInstances[circuit.routeFile.sectorCount] <optimize=false>; // "nombre de comportements associes a l'objet"
} EnvironmentFile;

typedef struct // ObjectInstanceList
{
	int instanceCount; // "nombre d'objets + nb de comportements"
	ObjectInstance instances[instanceCount] <optimize=false>;
} ObjectInstanceList;

typedef struct // ObjectInstance
{
	int objectIndex; // "nombre de comportements associes a l'objet"
	int eventCount;
	ObjectInstanceEvent events[eventCount];
	v3fix position;
	m33fix rotation;
} ObjectInstance;

typedef struct // ObjectInstanceEvent
{
	int eventIndex;
	int unknown;
} ObjectInstanceEvent;

#endif
