#ifndef A2D_H
#define A2D_H

#include "../math.h"
#include "../pbdf.h"
#include "../texture.h"

struct Ani2D;
struct Ani2DFrame;
struct Ani2DKey;
struct Ani2DSprite;

typedef struct // Ani2D (.a2d)
{
	int startFrame;
	int frameCount; // "NbFrame"
	int spriteCount; // "NbSprite"
	int spriteCountTotal; // "NbSpriteTotal"

	pstring textureProjectName; // texture with this name must be loaded previously, or game crashes
	TextureProject textureProject(256) <optimize=false>; // "Fichier descripteur de textures"

	Ani2DSprite sprites[spriteCount]; // "Description des sprites"
	Ani2DFrame frames[frameCount] <optimize=false>; // "Description des frames"
} Ani2D;

typedef struct // Ani2DSprite
{
	int pageIndex; // "numero de page"
	int left, top, right, bottom; // "coords de l'image"
} Ani2DSprite;

typedef struct // Ani2DFrame
{
	v3fix value;
	int keyCount;
	Ani2DKey keys[keyCount];
} Ani2DFrame;

typedef struct // Ani2DKey
{
	int spriteIndex; // "num sprite"
	int width; // "largeur"
	int height; // "hauteur"
	int translationX; // "translation"
	int translationY; // "translation"
} Ani2DKey;

#endif
