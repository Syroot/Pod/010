#ifndef PIT_H
#define PIT_H

#include "../math.h"

struct PitFile;

typedef struct // PitFile (.pit)
{
	int count;
	v3fix positions[count];
	fix repairSeconds;
} PitFile;

#endif
