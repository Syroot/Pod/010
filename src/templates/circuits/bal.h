#ifndef BAL_H
#define BAL_H

#include "../math.h"
#include "common.h"

struct Beacon;
struct BeaconFile;
struct Checkpoint;

typedef struct // BeaconFile (.bal)
{
	EventList eventList;

	int beaconCount; // "Nombre de balises"
	Beacon beacons[beaconCount];

	if (!OEM)
	{
		int checkpointCount; // "Nombre de chronos"
		Checkpoint checkpoints[checkpointCount];
	}
} BeaconFile;

typedef struct // Beacon
{
	v3fix points[4]; // "Point", vertex locations of passing plane
	v3fix normal; // "Normale", normal of resulting face
	int positiveEvent; // "No Evenements sens positif", index of event in event file when passed forwards
	int negativeEvent; // "No Evenements sens negatif", index of event in event file when passed backwards
} Beacon <read=BeaconRead>;
string BeaconRead(Beacon& value)
{
	if (value.positiveEvent != -1 && value.negativeEvent != -1)
		return Str("P=%d, N=%d", value.positiveEvent, value.negativeEvent);
	else if (value.positiveEvent != -1)
		return Str("P=%d", value.positiveEvent);
	else if (value.negativeEvent != -1)
		return Str("N=%d", value.negativeEvent);
	return "";
}

typedef struct // Checkpoint
{
	int beaconIndex; // index of beacon in beacon list
	short positive; // bool
	short distanceToNext;
	v3fix mapLineFrom;
	v3fix mapLineTo;
} Checkpoint <read=CheckpointRead>;
string CheckpointRead(Checkpoint& value)
{
	return Str("Beacon %d, %c, %d", value.beaconIndex, value.positive ? 'P' : 'N', value.distanceToNext);
}

#endif
