#ifndef TIM_H
#define TIM_H

#include "../math.h"

typedef struct // TimeFile (.tim)
{
    int hasLapTimes; // bool
    fix lapMin;
    fix lapAvg;
    int hasPartTimes; // bool
    fix partsMin[5];
    fix partsAvg[5];
} TimeFile;

#endif
