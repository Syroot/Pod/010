#ifndef OBJECT_H
#define OBJECT_H

#include "../math.h"
#include "../pbdf.h"
#include "../texture.h"

struct Contact;
struct Decor;
struct Face;
struct Object;
struct Prism;

typedef struct(bool hasTextures) // Decor
{
	pstring filename;

	if (hasTextures)
	{
		pstring textureProjectName;
		TextureProject textureProject(VOODOO ? 128 : 256);
	}

	int hasFaceMaterial;
	Object object(hasFaceMaterial, false, true);

	Prism prism;

	int totalMass; // "Masse Objet"
	v3fix inertiaMatDiag; // "Diagonale Matrice Inertie"
	int resistance; // "Points de vie"
	int friction; // "Coef frottement visqueux (en test)"
	int contactCount; // "Nb de points de contact"
	Contact contacts[contactCount]; // "jeu pour x point de contact"
} Decor <read=DecorRead>;
string DecorRead(Decor& value)
{
	return PStr(value.filename);
}

typedef struct(bool hasFaceMaterial, bool hasFaceMirror, bool hasFaceProperty) // Object
{
	int vertexCount; // "Nombre de points dans le secteur"
	v3fix positions[vertexCount]; // "Liste des points : X Y Z"
	int faceCount; // "Nombre de faces"
	int triCount; // "Nombre de triangles"
	int quadCount; // "Nombre de quadrilateres"
	Face faces(hasFaceMaterial, hasFaceMirror, hasFaceProperty)[faceCount] <optimize=false>;
	v3fix normals[vertexCount];
	fix radius; // length of bounding box (prism) extent
} Object;

typedef struct(bool hasMaterial, bool hasMirror, bool hasProperty) // Face
{
	if (hasMaterial)
		pstring materialName; // "Nom du materiau sous 3ds"

 	// "Nombre de de points dans la face"
	if (PBDF_KEY == 0x5CA8 || PBDF_KEY == 0xD13F)
	{
		int index3;
		int index0;
		int indexCount;
		int index2;
		int index1;
	}
	else
	{
		int indexCount;
		int index0;
		int index1;
		int index2;
		int index3;
	}

	v3fix normal;

	pstring effect; // "Effet", FLAT, GOURAUD, TEXTURE, TEXGOU
	struct EffectData
	{
		if (PStr(effect) == "FLAT" || PStr(effect) == "GOURAUD")
			uint color;
		else
			int texIndex;
		v2u texUvs[4]; // tris only use 3
		int unknown; // apparently unused
		if (indexCount == 4)
			int unknownQuad[3]; // apparently unused
	} effectData;

	if (hasMirror && (normal.x || normal.y || normal.z))
		int mirror; // byte
	if (hasProperty)
	{
		// "Propriete", 3 bytes
		uint propVisible : 1;
		uint : 2;
		uint propRoad : 1;
		uint : 1;
		uint propWall : 1;
		uint propLod : 2;
		uint propTrans : 1;
		uint : 1;
		uint propBackface : 1;
		uint : 2;
		uint propDural : 1;
		uint : 2;
		uint propSlip : 2;
	}
} Face <read=FaceRead>;
string FaceRead(Face& value)
{
	return exists(value.materialName) ? PStr(value.materialName) : "";
}

typedef struct // Prism
{
	v3fix extent;
	fix length;  // hypotenuse of 2d extent, e.g. sqrt(extent.x * extent.x + extent.y * extent.y)
	v3fix center;
} Prism;

typedef struct // Contact
{
	fix radius; // "Rayon Point de Contact"
	v3fix center; // "Centre Local X Y Z"
	ubyte unused10[12] <fgcolor=0x888888, hidden=true>;
	int mass; // "Masse Point de Contact"
	ubyte unused32[32] <fgcolor=0x888888, hidden=true>;
} Contact;

#endif
