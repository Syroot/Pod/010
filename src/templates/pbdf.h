#ifndef PBDF_H
#define PBDF_H

typedef struct // PbdfHeader
{
	uint fileSize <format=hex>;
	uint offsetCount;
	uint offsets[offsetCount];
} PbdfHeader <fgcolor=0x0000FF>;

void PbdfSeekOffset(uint idxOffset)
{
	local uint headerSize = header.offsets[0];
	local uint offset = header.offsets[idxOffset];
	FSeek(offset - (offset / PBDF_BUFSIZE * sizeof(uint)));
}

typedef struct // pstring
{
	ubyte len;
	ubyte data[len];
} pstring <fgcolor=0x000088, read=pstringRead, write=pstringWrite>;
string pstringRead(pstring& value)
{
	int64 pos = startof(value);
	ubyte len = ReadUByte(pos++);
	if (len == 0)
		return "";
	char s[len];
	int i;
	for (i = 0; i < len; ++i)
		s[i] = ReadUByte(pos++) ^ ~i;
	return s;
}
void pstringWrite(pstring& value, string s)
{
	int64 pos = startof(value);
	ubyte len = Strlen(s);
	DeleteBytes(pos, 1 + value.len);
	InsertBytes(pos, 1 + len);
	WriteUByte(pos++, len);
	int i;
	for (i = 0; i < len; ++i)
		WriteUByte(pos++, s[i] ^ ~i);
}

string PStr(const pstring& v)
{
	if (!v.len)
		return "";
	local char s[v.len];
	local int i;
	for (i = 0; i < v.len; ++i)
		s[i] = v.data[i] ^ ~i;
	return s;
}

#endif
