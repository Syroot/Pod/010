#ifndef SOUND_H
#define SOUND_H

struct SoundEvent;

typedef struct // SoundEvent
{
    ushort id : 15;
    ushort type : 1;
} SoundEvent <read=SoundEventRead>;
string SoundEventRead(SoundEvent& value)
{
	if (value.type == 1 && value.id == 0x7FFF)
		return "no event";
	else if (value.type == 1)
		return Str("patch event %d", value.id);
	else
		return Str("fix event %d", value.id);
}

#endif
