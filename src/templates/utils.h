string SPrintfIntArray(int array[], int length)
{
	int i = 0;
	string s = "[";
	while (i < length)
		s += Str("%d" + (i < length - 1 ? ", " : "]"), array[i++]);
	return s;
}
