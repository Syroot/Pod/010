#ifndef MATH_H
#define MATH_H

struct m33fix;
struct v3fix;

// ---- bool ----

typedef ubyte bool <read=boolRead>;
string boolRead(bool& v)
{
	return v ? "true" : "false";
}
#define false 0
#define true 1

// ---- fix ----

typedef int fix <read=fixRead, write=fixWrite>;
string fixRead(fix& v)
{
	return Str("%f", fix2float(v));
}
void fixWrite(fix& value, string s)
{
	float f;
	SScanf(s, "%f", f);
	value = float2fix(f);
}

fix float2fix(float v)
{
	return v * (1 << 16);
}
float fix2float(fix v)
{
	return v / (float)(1 << 16);
}

// ---- rgb ----

typedef struct // rgb
{
	ubyte r, g, b;
} rgb <read=rgbRead, write=rgbWrite>;
string rgbRead(rgb& v)
{
	return Str("(%u, %u, %u)", v.r, v.g, v.b);
}
void rgbWrite(rgb& v, string s)
{
	SScanf(s, "(%u, %u, %u)", v.r, v.g, v.b);
}

// ---- rgb555 ----

typedef struct // rgb555
{
	ushort b : 5;
	ushort g : 5;
	ushort r : 5;
} rgb555 <read=rgb555Read, write=rgb555Write>;
string rgb555Read(rgb555& v)
{
	const float f5 = 0b11111111 / (float)0b11111;
	ubyte r = v.r * f5;
	ubyte g = v.g * f5;
	ubyte b = v.b * f5;
	return Str("(%u, %u, %u)", r, g, b);
}
void rgb555Write(rgb555& v, string s)
{
	const float f5 = 0b11111 / (float)0b11111111;
	ubyte r, g, b;
	SScanf(s, "(%u, %u, %u)", r, g, b);
	v.r = Ceil(r * f5);
	v.g = Ceil(g * f5);
	v.b = Ceil(b * f5);
}

// ---- v2u ----

typedef struct // v2u
{
	uint x, y;
} v2u <read=v2uRead, write=v2uWrite>;
string v2uRead(v2u& v)
{
	return Str("(%u, %u)", v.x, v.y);
}
void v2uWrite(v2u& v, string s)
{
	SScanf(s, "(%u, %u)", v.x, v.y);
}

// ---- v2fix ----

typedef struct // v2fix
{
	fix x, y;
} v2fix <read=v2fixRead, write=v2fixWrite>;
string v2fixRead(v2fix& v)
{
	return Str("(%s, %s)", fixRead(v.x), fixRead(v.y));
}
void v2fixWrite(v2fix& v, string s)
{
	float x, y;
	SScanf(s, "(%f, %f)", x, y);
	v.x = float2fix(x); v.y = float2fix(y);
}

// ---- v3fix ----

typedef struct // v3fix
{
	fix x, y, z;
} v3fix <read=v3fixRead, write=v3fixWrite>;
string v3fixRead(v3fix& v)
{
	return Str("(%s, %s, %s)", fixRead(v.x), fixRead(v.y), fixRead(v.z));
}
void v3fixWrite(v3fix& v, string s)
{
	float x, y, z;
	SScanf(s, "(%f, %f, %f)", x, y, z);
	v.x = float2fix(x); v.y = float2fix(y); v.z = float2fix(z);
}

// ---- m33fix ----

typedef struct // m33fix
{
	fix m11, m12, m13;
	fix m21, m22, m23;
	fix m31, m32, m33;
} m33fix <read=m33fixRead, write=m33fixWrite>;
string m33fixRead(m33fix& v)
{
	return Str("((%s, %s, %s), (%s, %s, %s), (%s, %s, %s))",
		fixRead(v.m11), fixRead(v.m12), fixRead(v.m13),
		fixRead(v.m21), fixRead(v.m22), fixRead(v.m23),
		fixRead(v.m31), fixRead(v.m32), fixRead(v.m33));
}
void m33fixWrite(m33fix& v, string s)
{
	float m11, m12, m13;
	float m21, m22, m23;
	float m31, m32, m33;
	SScanf(s, "((%f, %f, %f), (%f, %f, %f), (%f, %f, %f))",
		m11, m12, m13,
		m21, m22, m23,
		m31, m32, m33);
	v.m11 = float2fix(m11); v.m12 = float2fix(m12); v.m13 = float2fix(m13);
	v.m21 = float2fix(m21); v.m22 = float2fix(m22); v.m23 = float2fix(m23);
	v.m31 = float2fix(m31); v.m32 = float2fix(m32); v.m33 = float2fix(m33);
}

#endif
