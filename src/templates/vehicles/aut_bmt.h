#ifndef AUT_H
#define AUT_H

#include "../math.h"

struct Auto;
struct AutoPart;
struct Motor;

typedef struct // Auto (.aut/.bmt)
{
    AutoPart wheelFR;
    AutoPart wheelRR;
    AutoPart wheelFL;
    AutoPart wheelRL;
    AutoPart chassis;

    Motor motor;
    int motorCurveCount; // "Nombre de valeurs dans le tableau" (.bmt)
    int motorCurves[motorCurveCount]; // "Valeurs du tableau Courbe_Moteur" (.bmt)
} Auto;

typedef struct // AutoPart
{
    ubyte runtime0[60] <fgcolor=0x888888, hidden=true>;
    fix carSuspension; // "Amortisseur voiture" (.aut), 0
    fix wheelMass; // "Masse des roues" (.aut), 0-2500.0 (front), 0-500 (rear)
    ubyte runtime44[4] <fgcolor=0x888888, hidden=true>;
    fix wheelPositionX; // "Position des Roues" (.aut), 0-4 (front), -4-0 (rear)
    fix wheelPositionY; // "Position des Roues" (.aut), 0-2
    fix wheelChassisHeight; // "Hauteur du chassis par rapport aux roues" (.aut), 0
    fix wheelRadius; // "Rayon des roues" (.aut) (0-1)
    ubyte runtime58[56] <fgcolor=0x888888, hidden=true>;
    fix wheelFriction; // "Frottement des roues (plus R tourne vite, + R freine)" (.aut), 0-10
    fix wheelGripX; // "Adherence Roues ( x )" (.aut), 0-4
    fix wheelGripY; // "Adherence Roues ( y )" (.aut), 0-0.610352
    fix wheelGripZ; // "Adherence Roues ( z )" (.aut), 0
    ubyte runtimeA0[12] <fgcolor=0x888888, hidden=true>;
    fix wheelSuspensionStiffness; // "Amortisseurs Roues ( Raideur )" (.aut), 0-1
    fix wheelSuspensionViscosity; // "Amortisseurs Roues ( Visquosité )" (.aut), 0-0.076599
    fix wheelSuspensionZEmpty; // "Amortisseurs Roues ( Z vide )" (.aut), -0.592697-0.5
    fix wheelSuspensionZMax; // "Amortisseurs Roues ( Z Max )" (.aut), 0-1
    fix wheelSuspensionZMin; // "Amortisseurs Roues ( Z Min )" (.aut), -1-0
    ubyte runtimeC0[24] <fgcolor=0x888888, hidden=true>;
} AutoPart;

typedef struct // Motor
{
    ubyte unused438[19] <fgcolor=0x888888, hidden=true>;
    ubyte speedFactorCount; // "Nombre de vitesses Utilisees (V.Nombre_Rapports)" (.bmt), 0-8
    fix speedFactors[9]; // "Coefficients des vitesses" (.bmt)
    ubyte unused46C[20] <fgcolor=0x888888, hidden=true>;
    fix steerMax; // "Max braquage" (.aut), 0-1.569092
    fix steerSpeed; // "Vit de braquage" (.aut), 0-0.399292
    fix steerRecall; // "Rappel de braquage" (.aut), 0-2
    ubyte unused490[OEM ? 8 : 4] <fgcolor=0x888888, hidden=true>; // OEM bytes are between steerRecall and gearUpSpeed
    fix unknown494; // unavailable 5th %d in "Vit de braquage,Delai de rappel,Max braquage,Rappel de braquage"
    ubyte unused498[112] <fgcolor=0x888888, hidden=true>;
    fix inertiaMomentsJx; // "Moments d'Inertie de la Voiture ( Jx )" (.aut), 0-6103.515625
    fix inertiaMomentsJy; // "Moments d'Inertie de la Voiture ( Jy )" (.aut), 0-6103.515625
    fix inertiaMomentsJz; // "Moments d'Inertie de la Voiture ( Jz )" (.aut), 0-6103.515625
    ubyte unused514[4] <fgcolor=0x888888, hidden=true>;
    fix gearUpSpeed; // "Vitesse d'augmentation du Regime Commandé" (.bmt)
    fix gearDownSpeed; // "Vitesse de diminution du Regime Commandé" (.bmt)
    fix chassisMass; // "Masse du chassis de la Voiture" (.aut), 0-3000
    ubyte unused524[4] <fgcolor=0x888888, hidden=true>;
    fix gravityFactor; // "Coefficient Gravité" (.aut), 0-5
    ubyte unused52C[4] <fgcolor=0x888888, hidden=true>;
    fix finFactorX; // "Coefficient des ailerons en X" (.aut), 0
    fix finFactorY; // "Coefficient des ailerons en Y" (.aut), 0
    fix brakeDistribution; // "Repartition du freinage" (.aut), 0-100
    fix zoomFactor; // "Coefficient de Zoom" (.aut), 0-1.601898
    fix viscousFriction; // "Resistance à l'avancement (frottement Visqueux)" (.aut), 0
    fix brakeSlopeCurve; // "Pente Courbe Freinage" (.aut), 0-3.051758
    fix brakeMax; // "Freinage Max" (.aut), 0-3.051758
    int transmissionType; // "Type de transmission (1=4RM, 2=Traction, 3=Propulsion)" (.aut)
    ubyte unused550[8] <fgcolor=0x888888, hidden=true>;
} Motor;

#endif
