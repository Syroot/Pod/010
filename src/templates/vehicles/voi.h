#ifndef VOI_H
#define VOI_H

#include "../light.h"
#include "../object.h"
#include "../pbdf.h"
#include "../texture.h"
#include "aut_bmt.h"
#include "v3d.h"
#include "vfx.h"

struct Vehicle;
struct VehicleUnknown;

typedef struct // Vehicle (.voi)
{
    if (BV < 6)
	    pstring name; // "Modele de voiture"

    if (BV < 8)
    {
        Auto auto <bgcolor=0xCDE6FF>; // "fichier de parametres" and "fichier bloc moteur"
        VehicleGraphics graphics <bgcolor=0xCDCDFF>; // "fichier graphique"
        Decor lod1(false) <bgcolor=0xEFCDFF>; // "fichier voiture simplifiee"
        Decor lod2(false) <bgcolor=0xEFCDFF>; // "fichier voiture simplifiee"
    }
    else
    {
        VehicleGraphics graphics <bgcolor=0xCDCDFF>; // "fichier graphique"
        Decor lod1(false) <bgcolor=0xEFCDFF>; // "fichier voiture simplifiee"
        Decor lod2(false) <bgcolor=0xEFCDFF>; // "fichier voiture simplifiee"
        Auto auto <bgcolor=0xCDE6FF>; // "fichier de parametres" and "fichier bloc moteur"
    }
    VehicleSounds sounds <bgcolor=0xFFCDCD>; // "Nom du fichier bruitages"

    // "Settings par defaut"
    if (BV < 8)
    {
        int defaultAccel;
        int defaultBrakes;
        int defaultGrip;
        int defaultHandling;
        int defaultSpeed;
    }
    else
    {
        int defaultSpeed;
        int defaultHandling;
        int defaultGrip;
        int defaultBrakes;
        int defaultAccel;
        int defaultBrakes;
        int defaultHandling;
        int defaultSpeed;
        int defaultGrip;
        int defaultAccel;
    }

    Light light;

    if (BV > 5)
        pstring name; // "Modele de voiture"
} Vehicle <bgcolor=0xCDFFFF>;

#endif
