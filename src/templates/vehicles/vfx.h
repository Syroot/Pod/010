#ifndef VFX_H
#define VFX_H

#include "../sound.h"

struct VehicleSounds;

typedef struct // VehicleSounds (.vfx)
{
  ubyte runtime0[4] <fgcolor=0x888888, hidden=true>;
  SoundEvent engine;
  SoundEvent gearNext;
  SoundEvent gearPrev;
  SoundEvent brake;
  SoundEvent brakeStop;
  SoundEvent crash_soft;
  SoundEvent unknown;
  SoundEvent shock;
  SoundEvent accel;
  SoundEvent accelStop;
  SoundEvent decel;
  SoundEvent decelStop;
  SoundEvent crash;
  SoundEvent skidRoof;
  SoundEvent skidRoofStop;
  ubyte unused22[2] <fgcolor=0x888888, hidden=true>;
} VehicleSounds;

#endif
