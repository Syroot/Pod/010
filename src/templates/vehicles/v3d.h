#ifndef V3D_H
#define V3D_H

#include "../object.h"
#include "../pbdf.h"

struct VehicleGraphics;
struct VehicleGraphicsObject;

typedef struct // VehicleGraphics (.v3d)
{
    pstring textureProjectName;
    TextureProject textureProject(VOODOO ? 128 : 256); // "Nom du fichier graphique"

    int hasFaceMaterial; // bool
    VehicleGraphicsObject chassisRR0(hasFaceMaterial);
    VehicleGraphicsObject chassisRL0(hasFaceMaterial);
    VehicleGraphicsObject chassisSR0(hasFaceMaterial);
    VehicleGraphicsObject chassisSL0(hasFaceMaterial);
    VehicleGraphicsObject chassisFR0(hasFaceMaterial);
    VehicleGraphicsObject chassisFL0(hasFaceMaterial);
    VehicleGraphicsObject chassisRR1(hasFaceMaterial);
    VehicleGraphicsObject chassisRL1(hasFaceMaterial);
    VehicleGraphicsObject chassisSR1(hasFaceMaterial);
    VehicleGraphicsObject chassisSL1(hasFaceMaterial);
    VehicleGraphicsObject chassisFR1(hasFaceMaterial);
    VehicleGraphicsObject chassisFL1(hasFaceMaterial);
    VehicleGraphicsObject chassisRR2(hasFaceMaterial);
    VehicleGraphicsObject chassisRL2(hasFaceMaterial);
    VehicleGraphicsObject chassisSR2(hasFaceMaterial);
    VehicleGraphicsObject chassisSL2(hasFaceMaterial);
    VehicleGraphicsObject chassisFR2(hasFaceMaterial);
    VehicleGraphicsObject chassisFL2(hasFaceMaterial);
    VehicleGraphicsObject wheelFR(hasFaceMaterial);
    VehicleGraphicsObject wheelRR(hasFaceMaterial);
    VehicleGraphicsObject wheelLF(hasFaceMaterial);
    VehicleGraphicsObject wheelLR(hasFaceMaterial);
    if (OEM)
    {
        Object shadow(hasFaceMaterial, false, true);
    }
    else
    {
        Object shadowR0(hasFaceMaterial, false, true);
        Object shadowF0(hasFaceMaterial, false, true);
        Object shadowR2(hasFaceMaterial, false, true);
        Object shadowF2(hasFaceMaterial, false, true);
    }
} VehicleGraphics;

typedef struct(bool hasFaceMaterial) // VehicleGraphicsObject
{
    Object object(hasFaceMaterial, false, true);
    Prism prism;
} VehicleGraphicsObject;

#endif
