#ifndef LIGHT_H
#define LIGHT_H

#include "../math.h"

typedef struct // Light
{
	enum
	{
		LIGHT_CYLINDER,
		LIGHT_SPHERE,
		LIGHT_CONE,
	} type; // "Type de Faisceau"
	v3fix position;
	m33fix rotation;
	fix radius; // "Rayon"
	fix intensity; // "Intensite"
	enum
	{
		DIFFUSE_NONE,
		DIFFUSE_LINEAR,
		DIFFUSE_SQUARE,
	} diffusionType; // "Type de Diffusion"
	fix angle; // "Angle"
} Light;

#endif
