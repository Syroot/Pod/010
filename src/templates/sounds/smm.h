#ifndef SMM_H
#define SMM_H

#include "../math.h"
#include "common.h"

typedef int rpm <read=Str("%f", this * 600.0 / 0xFFFF), write=(this = Atof(value) / 600.0 * 0xFFFF)>;

typedef struct
{
    char magic[4];
    ushort splitCount;
    ushort lastSplitIndex;

    struct SmmSplit
    {
        int id;
        int rangeCount;
        char description[20];
        if (rangeCount > 0)
        {
            struct SmmRange
            {
                ushort resource;
                ushort volMax;
                rpm inA;
                rpm inB;
                rpm inC;
                rpm inD;
                fix outB;
                fix outC;
                BOOL pitch;
            } ranges[rangeCount] <read=Str("resource %d", resource)>;
        }
    } splits[splitCount] <optimize=false, read=description>;
} Smm;

#endif
