#ifndef COMMON_H
#define COMMON_H

typedef int BOOL <read=(this ? "TRUE" : "FALSE")>;

typedef struct
{
    ubyte day;
    ubyte month;
    ubyte year;
    ubyte hour;
    ubyte minute;
    ubyte second;
} ReservoirTime <read=Str("%02d/%02d/%02d %02d:%02d:%02d", day, month, year, hour, minute, second)>;

typedef struct
{
    char magic[4];
    ReservoirTime created;
    ReservoirTime modified;
    char description[0xFF];
    char project[81];
    ushort versionMinor;
    ushort versionMajor;
} ReservoirHeader;

#endif
