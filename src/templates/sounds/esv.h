#ifndef ESV_H
#define ESV_H

#include "common.h"

typedef struct
{
    ReservoirHeader header;
    ushort eventCount;
    ushort lastEventIndex;
    ubyte typeCount;
    ubyte gap <hidden=true>;

    struct EsvType
    {
        char description[20];
        struct EsvParam
        {
            char name[19];
            enum <ubyte> EsvParamType
            {
                Resource,
                Event,
                Full,
                Calculation,
            } type;
        } params[3] <read=name>;
    } types[typeCount] <read=description>;

    struct EsvEvent
    {
        ushort id;
        char name[80];
        char author[14];
        int type;
        int param1;
        int param2;
        int param3;
        BOOL looping;
        BOOL spatial;
        BOOL doppler;
        BOOL dynamic;
        BOOL reverbable;
        BOOL stoppable;
    } events[eventCount] <read=name>;
} Esv;

#endif
