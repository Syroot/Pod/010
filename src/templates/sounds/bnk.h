#ifndef BNK_H
#define BNK_H

#include "../sound.h"

typedef struct
{
    ushort idCount;
    SoundEvent ids[idCount];
} Bnk;

#endif
