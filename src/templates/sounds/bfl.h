#ifndef BFL_H
#define BFL_H

#include "rsv.h"

typedef struct(Rsv& rsv)
{
    local int64 start = FTell();
    local int r;
    local int f;
    for (r = 0; r < rsv.resourceCount; ++r)
    {
        for (f = 0; f < rsv.formatCount; ++f)
        {
            if (rsv.resources[r].params[f].size)
            {
                struct BflSample
                {
                    local string file = rsv.resources[r].params[f].file;
                    FSeek(start + rsv.resources[r].params[f].ptr);
                    if (rsv.formats[f].bits == 8)
                        ubyte pcm[rsv.resources[r].params[f].size];
                    else
                        ushort pcm[rsv.resources[r].params[f].size / 2];
                } samples <read=file>;
            }
        }
    }
} Bfl;

#endif
