#ifndef RSV_H
#define RSV_H

#include "common.h"

typedef struct
{
    ReservoirHeader header;
    ushort resourceCount;
    ushort lastResourceIndex;
    ushort formatCount;

    struct RsvFormat
    {
        short bits;
        short hz;
    } formats[formatCount] <read=Str("%d Bits, %d Hz", bits, hz)>;

    struct RsvResource
    {
        char description[256];
        ushort id;
        enum <ushort> RsvResourceType
        {
            Sample = 1,
            Midi,
            CD,
            Sequence,
            Split
        } type;
        BOOL pitchable;
        BOOL volable;
        BOOL panable;
        BOOL spacable;
        BOOL reverbable;
        BOOL loadable;
        struct RsvParam
        {
            char file[21];
            ubyte volume;
            ushort align <hidden=true>;
            BOOL isLooping;
            int ptr;
            int size;
            int frequency;
            int startLoop;
            int loopLength;
        } params[formatCount] <read=file>;
    } resources[resourceCount] <optimize=false, read=description>;
} Rsv;

#endif
