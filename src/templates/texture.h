#ifndef TEXTURE_H
#define TEXTURE_H

struct Texture;
struct TexturePage;
struct TextureProject;
struct TextureRegion;

typedef struct(int size) // TextureProject
{
	int textureCount; // .chp
	int paletteCount; // .chp, always set to 1 and unused
	Texture textures[textureCount] <optimize=false>; // .chp

	if (!VOODOO)
		rgb palette[256]; // .pls

	TexturePage texturePages(size)[textureCount] <optimize=true>; // .pag
} TextureProject;

typedef struct // Texture
{
	int regionCount;
	TextureRegion regions[regionCount];
} Texture;

typedef struct // TextureRegion
{
	char name[32];
	int left, top, right, bottom;
	int index;
} TextureRegion <read=TextureRegionRead>;
string TextureRegionRead(TextureRegion& value)
{
	return value.name;
}

typedef struct(int size) // TexturePage
{
	ubyte data[size * size * (VOODOO ? 2 : 1)]; // PAL8 : RGB565
} TexturePage;

#endif
