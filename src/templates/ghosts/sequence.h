#ifndef SEQUENCE_H
#define SEQUENCE_H

#include "../math.h"

struct Sequence;
struct SequenceHeader;
struct SequenceInfo;
struct SequencePoint;

typedef struct(int size) // Sequence
{
    local int64 offset = FTell();
    uint magic0 <format=hex>; // 0x12345678
    uint magic1 <format=hex>; // 0x78451236
    int version; // 2
    int dataEnd; // followed by points, relative to start of SEQ
    int dataStart; // preceeded by headers, relative to start of SEQ
    int headerCount;
    int unknown; // -1
    SequenceHeader headers[headerCount] <optimize=false>;
    int dataSize;
    SequenceInfo info; // ubyte data[dataSize];
    SequencePoint points[(size - (FTell() - offset)) / sizeof(SequencePoint)];
} Sequence;

typedef struct // SequenceHeader
{
    int type;
    int size;
    ubyte data[size];
} SequenceHeader;

typedef struct // SequenceInfo
{
    int unknown0;
    int circuitIndex; // dependent on client circuit order
    fix time;
    int unknown1[7];
    fix raceTime;
    fix startTime;
    fix lapTimes[3];
    fix partTimesLap1[4];
    fix partTimesLap2[4];
    fix partTimesLap3[4];
    int unknown2[6];
    char vehicleName[8];
    int unknown3[7];
    char playerName[8];
    ubyte unknown4[50];
    ubyte grip;     // ^ 124 / 0x7C / 0b01111100
    ubyte speed;    // ^ 189 / 0xBD / 0b10111101
    ubyte handling; // ^  58 / 0x3A / 0b00111010
    ubyte brakes;   // ^ 224 / 0xE0 / 0b11100000
    ubyte accel;    // ^  30 / 0x1E / 0b00011110
    ubyte unknown5;
    int unknown6[5];
} SequenceInfo;

typedef struct // SequencePoint
{
    fix time;
    int wheelRotation;
    int wheelSteer;
    fix wheelZ[4];
    v3fix position;
    v3fix rotationX;
    v3fix rotationY;
} SequencePoint;

#endif
